import { Component, OnInit } from '@angular/core';
import { People } from '../models/people';
import { PeopleService} from '../services/people.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  count: number = 1;
  characters :any;
  people:any;
  selected:any;
  constructor( private peopleService: PeopleService) { }

  ngOnInit() {

    this.peopleService.getPeople(this.count).subscribe( people => {
     this.characters = [];
     for(const person of  people.results){
       this.characters.push(person);
     }

    });
  }
  moreCharacters(): void {
    this.count++;
    this.peopleService.getPeople(this.count).subscribe(people => {
      for(const person of  people.results ){
        this.characters.push(person);
      }
    });
  };
  selectCharacter(person) {
     this.selected = person;
    console.log(this.selected);
  }
}
