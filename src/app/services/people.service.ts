import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { People } from '../models/people';
import { GLOBALS} from './globals';
import { Location } from '@angular/common';

@Injectable()
export class PeopleService {
  people: any;
constructor(private http: Http, private location: Location) { }
  getPeople(count): Observable <People> {
  let url;
  if(count > 1) {
     url = GLOBALS.apiUrl + 'people/?page='+ count;
  }
  else {
    url = GLOBALS.apiUrl + 'people/';

  }
  return this.http.get(url).pipe(
    map(response => {
      const res = response.json();
      const body = res;
      return body || {};
    }));
  }
}
