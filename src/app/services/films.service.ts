import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Films } from '../models/films';
import { GLOBALS} from './globals';
import { Location } from '@angular/common';


@Injectable()
	export class FilmsService {

	constructor(private http: Http, private location: Location) { }
	getFilms(): Observable <Films> {
		let url;
		url = GLOBALS.apiUrl + 'films' ;
		return this.http.get(url).pipe(
			map(response => {
				const res = response.json();
        const body = res;
        return body || {};
			}));
  		};
}
