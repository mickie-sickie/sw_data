import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PeopleComponent} from './people/people.component';
import { PlanetsComponent} from './planets/planets.component';
import { SpeciesComponent} from './species/species.component';
import { StarshipsComponent} from './starships/starships.component';
import { VehiclesComponent} from './vehicles/vehicles.component';

const routes: Routes = [
	{path : '', component: HomeComponent},
	{ path: 'people' , component: PeopleComponent},
	{path:  'planets' , component:PlanetsComponent},
	{path: 'species' , component:SpeciesComponent},
	{path:'starships', component:StarshipsComponent},
	{path: 'vehicles', component:VehiclesComponent },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
	exports: [RouterModule]
})
export class AppRoutingModule {

 }
export const routableComponents = [
	HomeComponent
];
