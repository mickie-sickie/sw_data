import { Component, OnInit } from '@angular/core';
import { Films } from '../models/films';
import { FilmsService } from '../services/films.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	  films: any;
  filmselected:any;
  constructor(private filmsService: FilmsService) { }
  ngOnInit() {
  	console.log('home works')
  	this.filmsService.getFilms().subscribe( films => {
  		this.films = films.results;
      console.log('films', this.films);
  	});
  }
  cardSelected(film) {
    this.filmselected = film;
    console.log('card seleccionado' ,this.filmselected);

  }
}
