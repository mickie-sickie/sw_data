import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';
import { PeopleComponent } from './people/people.component';
import { SpeciesComponent } from './species/species.component';
import { StarshipsComponent } from './starships/starships.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule , routableComponents} from './/app-routing.module';
import { HeaderComponent } from './header/header.component';
import { PlanetsComponent } from './planets/planets.component';
//SERVICE
import { FilmsService } from './services/films.service';
import {PeopleService} from './services/people.service';

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    PeopleComponent,
    SpeciesComponent,
    StarshipsComponent,
    VehiclesComponent,
    HomeComponent,
    HeaderComponent,
    PlanetsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [ FilmsService , PeopleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
