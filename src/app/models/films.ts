interface IFilms {
	title:string,
	episode_id: number;
	opening_crawl: string;
	director:string;
	release_date: string;
  results: [
    {
      title:string;
      episode_id:number;
      director: string;
      producer:string;
      release_date:string;
    }
    ];
}
export class Films implements IFilms{
	title:string;
	episode_id : number;
	opening_crawl: string;
	director:string;
  release_date: string;

  results: [
    {
      title:string;
      episode_id:number;
      director: string;
      producer:string;
      release_date: string;
    }
    ];

	constructor( films:any ) {
		this.title = films.title;
		this.episode_id = films.episode_id;
		this.opening_crawl =films.opening_crawl;
		this.director = films.firector;
		this.release_date = films.release_date;
	}
}
