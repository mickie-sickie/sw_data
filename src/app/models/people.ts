interface IPeople {
  name: string;
  birth_year: string;
  gender: string;
  results: [
    {
      name:string;
      height:string;
      hair_color: string;
      eye_color:string;
      gender:string;
    }
    ];
}
export class People implements IPeople {
  name: string;
  birth_year: string;
  gender: string;
  results: [
    {
      name:string;
      height:string;
      hair_color: string;
      eye_color:string;
      gender:string;
    }
  ];
  constructor( people: any ) {
  this.name = people.name;
  this.birth_year = people.birth_year;
  this.gender = people.gender;
  this.results = people.results;
  }
}
